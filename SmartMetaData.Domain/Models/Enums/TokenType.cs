namespace SmartMetaData.Domain.Models.Enums;

public enum TokenType
{
    Erc721,
    Erc1155,
}
